package com.example.hw1


import android.content.Context
import android.graphics.Bitmap
import android.graphics.Point
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.util.jar.Manifest


class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DownloadAndSaveImageTask(this).execute("https://alexdunndev.files.wordpress.com/2018/01/screen-shot-2018-01-23-at-10-16-58-am.png?w=825&h=510&crop=1")

        val cm = baseContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val networkInfo = cm.activeNetworkInfo
       

        if(networkInfo != null && networkInfo.isConnected) {
            Picasso.get().load("file://"+ "data/data/Hw1/files/Images/img.jpg").config(Bitmap.Config.RGB_565).fit().centerCrop().into(imageView);
        }


    }
}
